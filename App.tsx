/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect} from 'react';
import {SafeAreaView, StyleSheet, Text, TouchableOpacity} from 'react-native';
import OneSignal from 'react-native-onesignal';

const init = (): void => {
  console.log('###INITIALIZING###');
  console.log(OneSignal.getDeviceState());
  OneSignal.setAppId('fe347453-a445-4aca-9a35-e838fee55b29');
  OneSignal.promptForPushNotificationsWithUserResponse();
  OneSignal.setNotificationWillShowInForegroundHandler(
    notificationReceivedEvent => {
      console.log(
        'OneSignal: notification will show in foreground:',
        notificationReceivedEvent,
      );
      let notification = notificationReceivedEvent.getNotification();
      console.log('notification: ', notification);
      const data = notification.additionalData;
      console.log('additionalData: ', data);
      // Complete with null means don't show a notification.
      notificationReceivedEvent.complete(notification);
    },
  );
  OneSignal.setNotificationOpenedHandler(notification => {
    console.log('OneSignal: notification opened:', notification);
  });
};

const subscribe = (): void => {};

const App = () => {
  useEffect(() => {
    init();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity onPress={subscribe}>
        <Text>Subscribe</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});

export default App;
